#![no_std]
#![no_main]

extern crate panic_semihosting;

use cortex_m::singleton;
use cortex_m_rt::entry;

use nb::block; // Only used for slowing down the rainbow effect
use smart_leds::{
    hsv::{hsv2rgb, Hsv},
    RGB8,
};
use stm32f1xx_hal::{
    pac,
    prelude::*,
    spi::{Mode, Phase, Polarity, Spi},
    timer::Timer,
};
use smart_leds::SmartLedsWrite;
use stm32f1_ws2812::{led_spi_bit_amount, Leds};

const LED_AMOUNT: usize = 2;
const LED_BIT_AMOUNT: usize = led_spi_bit_amount(LED_AMOUNT);

#[entry]
fn main() -> ! {
    // Standard f1 setup
    let dp = pac::Peripherals::take().unwrap();

    let mut rcc = dp.RCC.constrain();
    let mut flash = dp.FLASH.constrain();
    let clocks = rcc.cfgr.sysclk(16_000_000.hz()).freeze(&mut flash.acr);
    let mut mapr = dp.AFIO.constrain(&mut rcc.apb2).mapr;
    let mut gpioa = dp.GPIOA.split(&mut rcc.apb2);

    // For a slower rainbow, we'll use a timer
    let mut timer = Timer::tim2(dp.TIM2, &clocks, &mut rcc.apb1).start_count_down(60.hz());

    // Set up the SPI peripheral
    let pins = (
        gpioa.pa5.into_alternate_push_pull(&mut gpioa.crl),
        gpioa.pa6.into_floating_input(&mut gpioa.crl),
        gpioa.pa7.into_alternate_push_pull(&mut gpioa.crl),
    );

    let spi_mode = Mode {
        polarity: Polarity::IdleLow,
        phase: Phase::CaptureOnFirstTransition,
    };
    let spi = Spi::spi1(
        dp.SPI1,
        pins,
        &mut mapr,
        spi_mode,
        3.mhz(),
        clocks,
        &mut rcc.apb2,
    );

    // Set up the DMA channels
    let dma_channels = dp.DMA1.split(&mut rcc.ahb);
    let spi_dma = spi.with_tx_dma(dma_channels.3);

    // Holds the colour values
    let mut current_color = [
        Hsv{hue: 0, sat: 255, val: 255},
        Hsv{hue: 10, sat: 255, val: 255}
    ];

    let data_buffer: &'static mut [u8; LED_BIT_AMOUNT] =
        singleton!(: [u8; LED_BIT_AMOUNT] = [0; LED_BIT_AMOUNT]).unwrap();

    let mut leds = Leds::new(data_buffer, spi_dma);

    loop {
        // Update the current color value
        for color in current_color.iter_mut() {
            color.hue = (color.hue + 1) % 254;
        }
        let rgb_iterator = current_color.iter().cloned().map(hsv2rgb);

        leds.write(rgb_iterator);

        block!(timer.wait()).unwrap();
        timer.start(60.hz());
    }
}
