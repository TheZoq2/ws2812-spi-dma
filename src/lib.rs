#![no_std]
use smart_leds::{SmartLedsWrite, RGB8};

use stm32f1xx_hal::dma::{Static, Transfer, TransferPayload, Transferable, TxDma, WriteDma, R};
use stm32f1xx_hal::spi::SpiTxDma;

const BYTES_PER_LED: usize = 8;

pub enum Error {
    /// The data buffer did not contain enough bits to store all LEDs
    /// use the led_spi_bit_amount function to compute how many bits are required
    DataBufferTooSmall,
}
pub type Result<T> = core::result::Result<T, Error>;

pub struct Leds<T> {
    // NOTE: The options are here for ownership purposes. The DMA transaction
    // will take ownership of the structs while they are in used, and hand them
    // back afterwards.
    data_buffer: Option<&'static mut [u8]>,
    spi: Option<T>,
}

impl<T> Leds<T> {
    pub fn new(data_buffer: &'static mut[u8], spi: T) -> Self {
        Self {
            data_buffer: Some(data_buffer),
            spi: Some(spi)
        }
    }
}

impl<SPI, REMAP, PINS, CHANNEL> SmartLedsWrite for Leds<SpiTxDma<SPI, REMAP, PINS, CHANNEL>>
where
    SpiTxDma<SPI, REMAP, PINS, CHANNEL>: WriteDma<&'static mut [u8], u8>,
    Transfer<R, &'static mut [u8], SpiTxDma<SPI, REMAP, PINS, CHANNEL>>:
        Transferable<&'static mut [u8], SpiTxDma<SPI, REMAP, PINS, CHANNEL>>,
{
    type Error = Error;
    type Color = RGB8;

    /// Writes the specified colors.
    /// If the data buffer is too small, an error is returned.
    fn write<T, I>(&mut self, iterator: T) -> Result<()>
    where
        I: Into<Self::Color>,
        T: Iterator<Item = I>,
    {
        // NOTE: Safe unwraps since we will put these back at the end of the function
        let data = self.data_buffer.take().unwrap();
        let spi = self.spi.take().unwrap();

        let count = led_spi_bit_pattern(iterator.map(|i| i.into()), data)?;

        let transfer = spi.write(&mut data[0..count]);

        let (data, spi) = transfer.wait();

        self.data_buffer = Some(data);
        self.spi = Some(spi);
        Ok(())
    }
}

/// Fills the specified buffer with data that should be transmitted using the SPI bus
/// to set the leds to the specified colour. Returns the amount of bytes to transmit.
/// If the buffer is too small, an error is returned
pub fn led_spi_bit_pattern(
    leds: impl IntoIterator<Item = RGB8>,
    mut output: &mut [u8],
) -> Result<usize> {
    let mut led_count = 0;
    for led in leds {
        led_count += 1;
        // Make sure we have enough space for the resulting bits. Since we don't know the
        // size of the iterator, we have to do this check every iteration
        if output.len() < BYTES_PER_LED * 3 {
            return Err(Error::DataBufferTooSmall);
        }
        set_from_byte(led.g, output);
        output = &mut output[BYTES_PER_LED..];
        set_from_byte(led.r, output);
        output = &mut output[8..];
        set_from_byte(led.b, output);
        output = &mut output[8..];
    }
    // We also need some 0 bytes at the end to signal the end of the transmission
    if output.len() < led_spi_bit_amount(0) {
        return Err(Error::DataBufferTooSmall);
    }
    // Set those bytes to zero
    for b in &mut output[0..led_spi_bit_amount(0)] {
        *b = 0;
    }
    Ok(led_spi_bit_amount(led_count))
}

fn set_from_byte(byte: u8, mut target: &mut [u8]) {
    for i in 0..BYTES_PER_LED {
        const MASK: u8 = 0b1000_0000;
        set_spi_byte((byte << i) & MASK == MASK, target);
        target = &mut target[1..]
    }
}

fn set_spi_byte(value: bool, target: &mut [u8]) {
    target[0] = match value {
        false => 0b10000000,
        true => 0b11110000,
    };
}

pub const fn led_spi_bit_amount(led_amount: usize) -> usize {
    // Each led needs 24 bits transfered
    // At 3 MHz, one pulse is 0.33333 us
    // The pattern for a 0 is
    // ^^^^^^^|________________
    // | 0.35 |      0.9      |
    // And for a 1
    // ^^^^^^^^^^^^^^^^|_______
    // |               | 0.35 |
    // That is, a 1 is 4 high pulses, followed by 1 low pulse
    // And a 0 is 1 low pulse followed by 4 low

    // The delay between bits can be fairly long, so for simplicity, one transfered byte will be
    // used per bit

    // This means that the total length of the transmitted data is 24*led_amount+reset time

    // Reset time is specified as 50 us which requires ~150 pulses. This is ~160/8=20 bits
    let reset_count = 20;

    led_amount * 24 + reset_count
}
